package 四则运算.test;

  //     测试中缀转后缀
import junit.framework.TestCase;
import org.junit.Test;
import 四则运算.src.InfixToSuffix;

public class InfixToSuffixTest extends TestCase {
    InfixToSuffix e1 = new InfixToSuffix();
    @Test
    public void testNifixToSuffix() throws Exception {
        e1.conversion("1 - 7 * ( 9 + 5 ) * 3");
        assertEquals("1 7 9 5 + * 3 * - ", e1.getMessage());
    }
}
