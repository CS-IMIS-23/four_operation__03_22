package 四则运算.test;

   //    测试有理数（分数）的计算
import junit.framework.TestCase;
import org.junit.Test;
import 四则运算.src.RationalNumber;

public class RationalNumberTest extends TestCase {
    RationalNumber r1 = new RationalNumber(5,9);
    RationalNumber r2 = new RationalNumber(3,5);
    @Test
    public void testGetNumerator() throws Exception {   //  测试获取分子
        assertEquals(5,r1.getNumerator());
    }
    @Test
    public void testGetDenominator() throws Exception {    //   测试获取分母
        assertEquals(9,r1.getDenominator());
    }
    @Test
    public void testReciprocal() throws Exception {   //    测试获取倒数
        assertEquals("9/5",(r1.reciprocal()).toString());
    }
    @Test
    public void testAdd() throws Exception {    //    测试加法

        assertEquals("52/45",(r1.add(r2)).toString());
    }
    @Test
    public void testSubtract() throws Exception {    //   测试减数
        assertEquals("-2/45",(r1.subtract(r2)).toString());
    }
    @Test
    public void testMultiply() throws Exception {   //    测试乘法
        assertEquals("1/3",(r1.multiply(r2)).toString());
    }
    @Test
    public void testDivide() throws Exception {    //    测试除法
        assertEquals("25/27",(r1.divide(r2)).toString());
    }
}
