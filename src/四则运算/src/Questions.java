package 四则运算.src;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Scanner;
//
public class Questions {
    public static void main(String[] args) throws FileNotFoundException {

        Scanner scan = new Scanner(System.in);
        Scanner number = new Scanner(System.in);
        MakeQuestions Nq = new MakeQuestions();

        PrintStream ps = new PrintStream("Exercises.txt");;

        int count, level;
        int q = 0;
        String expr;


        while(true)

        {
            System.out.print("请输入要生成的题目数：");
            count = scan.nextInt();
            while (count == 0)
            {
                System.out.println("错误，请输入有效数字！(最小为1，理论无上限)");
                System.out.print("请输入要生成的题目数：");
                count = number.nextInt();
            }
            System.out.print("请输入生成题目的级别（每增加一级多一个运算符，最低为一级）：");
            level = scan.nextInt();
            while (level == 0)
            {
                System.out.println("错误，请输入有效数字！(最小为1，理论无上限)");
                System.out.print("请输入生成题目的级别（每增加一级多一个运算符，最低为一级）：");
                level = number.nextInt();
            }
            System.out.println(" ");

                for (int i = 0; i < count; i++) {
                    //生成题目
                    int a;
                    a = i + 1;
                    expr = Nq.getExper(level);
                    String s = "题目" + a + ":" + expr + " =";
                    System.out.println(s);

                    ps.append(s);
                }
                break;

        }
        Process p=null;
        try {
            p=Runtime.getRuntime().exec("notepad.exe Exercises.txt");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
